import http from "http";
import fs from "fs";
import path from "path";
import { fileURLToPath } from "url";

const __dirname = path.dirname(fileURLToPath(import.meta.url));
const directory = path.join(__dirname, "../public");

const port = 3000;

http
  .createServer((req, res) => {
    switch (req.url) {
      case "/":
        req.url = "/index.html";
        break;
      case "/cars":
        req.url = "/cars.html";
        break;
    }
    let path = directory + req.url;
    fs.readFile(path, (err, data) => {
      if (path.includes(".svg")) {
        res.writeHead(200, { "Content-type": "image/svg+xml" });
      } else {
        res.writeHead(200);
      }
      res.end(data);
    });
  })
  .listen(port, () => {
    console.log(`Server listening on port ${port}`);
  });
