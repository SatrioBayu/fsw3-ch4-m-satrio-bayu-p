class Car extends Component {
  static list = [];

  static init(cars) {
    this.list = cars.map((car) => new this(car));
  }

  constructor(props) {
    super(props);
    let { id, plate, manufacture, model, image, rentPerDay, capacity, description, transmission, available, type, year, options, specs, availableAt } = props;
    this.id = id;
    this.plate = plate;
    this.manufacture = manufacture;
    this.model = model;
    this.image = image;
    this.rentPerDay = rentPerDay;
    this.capacity = capacity;
    this.description = description;
    this.transmission = transmission;
    this.available = available;
    this.type = type;
    this.year = year;
    this.options = options;
    this.specs = specs;
    this.availableAt = availableAt;
  }
  render() {
    return `
      <div class="col-md-4 my-2">
        <div class="card h-100">
          <div class="card-body">
            <img src="${this.image}" class="card-img" alt="..." />
            <h6 class="card-title mt-3 card-title-text">${this.manufacture}/${this.model}</h6>
            <h5 class="card-title mt-3 card-title-text">Rp ${this.rentPerDay} / hari</h5>
            <p class="card-text">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Dolore rem natus maxime harum assumenda illum quibusdam quis qui facere obcaecati quod officia, molestias recusandae eius ducimus voluptate possimus, quisquam nulla.
            </p>
            <h6><i class="bi bi-people" style="font-size: 20px;"></i>  ${this.capacity} orang</h6>
            <h6><i class="bi bi-gear" style="font-size: 20px;"></i>  ${this.transmission}</h6>
            <h6><i class="bi bi-calendar" style="font-size: 20px;"></i>  Tahun ${this.year}</h6>
            <div class="d-flex flex-column mt-3 align-items-stretch">
              <button class="btn btn-regis">Pilih Mobil</button>
            </div>
          </div>
        </div>
      </div>
    `;
  }
}
