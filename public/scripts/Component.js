class Component {
  constructor(props) {
    if (this.constructor === Component) {
      throw new Error("Cannot instantiate directly from abstract Class");
    }
  }
  render() {
    // Will be overrided in Car.js
  }
}
